<?php

namespace Drupal\Tests\sports_league\FunctionalJavascript;

use Behat\Mink\Element\NodeElement;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests match creation and edition.
 */
class MatchTest extends WebDriverTestBase {

  use NodeCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'sl_default_content',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The base helper service.
   *
   * @var \Drupal\sl_base\SlBaseHelper
   */
  protected $slBaseHelper;

  /**
   * Test match creation and edition.
   */
  public function testMatch() {
    $this->slBaseHelper = \Drupal::service('sl_base.helper');

    $admin_user = $this->createUser([
      'administer sl',
      'create sl_match content',
      'edit any sl_match content',
      'create sl_rosters',
      'edit sl_rosters',
      'create sl_match_moments',
      'edit sl_match_moments',
      'access sl_browser_competitions entity browser pages',
      'access sl_browser_competitions_editions entity browser pages',
      'access sl_browser_teams entity browser pages',
    ]);

    $this->drupalLogin($admin_user);
    $this->drupalGet('/node/add/sl_match');
    $page = $this->getSession()->getPage();
    $page->findField('Administrative title')->hasAttribute('disabled');
    $page->findField('Status')->selectOption('Played');
    $page->findField('Categories')->selectOption('--England Cup');
    $page->fillField('Score Home', '3');
    $page->fillField('Score Away', '2');

    // Add competition.
    $this->assertSession()->buttonExists('Select competitions')->press();
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->switchToIFrame('entity_browser_iframe_sl_browser_competitions_editions');
    $this->getSession()->getPage()->checkField('entity_browser_select[node:21]');
    $this->getSession()->getPage()->pressButton('Select competitions');
    $this->getSession()->switchToIFrame();
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Add home team.
    $page->find('css', '#edit-field-sl-match-team-home-wrapper')->pressButton('Select teams');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->switchToIFrame('entity_browser_iframe_sl_browser_teams');
    $this->getSession()->getPage()->checkField('entity_browser_select[node:16]');
    $this->getSession()->getPage()->pressButton('Select teams');
    $this->getSession()->switchToIFrame();
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Add away team.
    $page->find('css', '#edit-field-sl-match-team-away-wrapper')->pressButton('Select teams');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->switchToIFrame('entity_browser_iframe_sl_browser_teams');
    $this->getSession()->getPage()->checkField('entity_browser_select[node:15]');
    $this->getSession()->getPage()->pressButton('Select teams');
    $this->getSession()->switchToIFrame();
    $this->assertSession()->assertWaitOnAjaxRequest();

    $page->pressButton('Save');
    $this->assertSession()->pageTextContains('Roma x Torino');
    $this->assertSession()->pageTextContains('England cup');
    $this->assertSession()->pageTextContains('England league 2016/2017');

    // Assert teams.
    $match = $this->getNodeByTitle('Roma x Torino');
    $roma_team = 16;
    $torino_team = 15;

    $teams = $match->field_sl_teams->referencedEntities();
    $this->assertEquals('Roma x Torino', $match->field_sl_administrative_title->value);
    $this->assertCount(2, $teams);
    $team_ids = array_column($match->field_sl_teams->getValue(), 'target_id');
    $this->assertEquals([16, 15], $team_ids);
    $this->drupalGet($match->toUrl('edit-form'));

    // Add rosters home.
    $rosters = $page->find('css', '#inline-entity-form-field_sl_match_home_inirosters-form');
    $this->addRosters($roma_team, $rosters, 0, 5);
    // Add rosters away.
    $rosters = $page->find('css', '#inline-entity-form-field_sl_match_away_inirosters-form');
    $this->addRosters($torino_team, $rosters, 0, 5);
    // Add rosters subs home.
    $rosters = $page->find('css', '#inline-entity-form-field_sl_match_home_inisubs-form');
    $this->addRosters($roma_team, $rosters, 6, 9);
    // Add rosters subs away.
    $rosters = $page->find('css', '#inline-entity-form-field_sl_match_away_inisubs-form');
    $this->addRosters($torino_team, $rosters, 6, 9);
    // Add home coach.
    $rosters = $page->find('css', '#inline-entity-form-field_sl_match_home_coach-form');
    $this->addRosters($roma_team, $rosters, 12, 13);
    // Add away coach.
    $rosters = $page->find('css', '#inline-entity-form-field_sl_match_away_coach-form');
    $this->addRosters($torino_team, $rosters, 12, 13);

    // Add moments.
    $moments = $page->find('css', '#inline-entity-form-field_sl_match_home_moments-form');
    $this->addMatchMoment($roma_team, $moments, 'select[name="field_sl_match_home_moments[actions][bundle]"]', 'Goal', 3, 5);
    $this->addMatchMoment($roma_team, $moments, 'select[name="field_sl_match_home_moments[actions][bundle]"]', 'Goal', 4, 10);
    $this->addMatchMoment($roma_team, $moments, 'select[name="field_sl_match_home_moments[actions][bundle]"]', 'Substitution', 5, 55, 6);
    $moments = $page->find('css', '#inline-entity-form-field_sl_match_away_moments-form');
    $this->addMatchMoment($torino_team, $moments, 'select[name="field_sl_match_away_moments[actions][bundle]"]', 'Goal', 1, 46);
    $this->addMatchMoment($torino_team, $moments, 'select[name="field_sl_match_away_moments[actions][bundle]"]', 'Substitution', 2, 90, 3);
    $page->pressButton('Save');

    // Assert counts.
    $home_rosters = $page->find('css', '#field_sl_match_home_rosters');
    $this->assertCount(5, $home_rosters->find('css', 'table')->findAll('css', 'tr'));
    $home_subs = $page->find('css', '#field_sl_match_home_subs');
    $this->assertCount(3, $home_subs->find('css', 'table')->findAll('css', 'tr'));
    $away_rosters = $page->find('css', '#field_sl_match_away_rosters');
    $this->assertCount(5, $away_rosters->find('css', 'table')->findAll('css', 'tr'));
    $away_subs = $page->find('css', '#field_sl_match_away_subs');
    $this->assertCount(3, $away_subs->find('css', 'table')->findAll('css', 'tr'));
    $home_moments = $page->find('css', '#field_sl_match_home_moments');
    $this->assertCount(3, $home_moments->find('css', 'table')->findAll('css', 'tr'));
    $away_moments = $page->find('css', '#field_sl_match_away_moments');
    $this->assertCount(2, $away_moments->find('css', 'table')->findAll('css', 'tr'));

    // Home rosters.
    $roma_players = $this->slBaseHelper->findTeamPlayers($roma_team);
    $roma_player_keys = array_keys($roma_players);

    $torino_players = $this->slBaseHelper->findTeamPlayers($torino_team);
    $torino_player_keys = array_keys($torino_players);

    $row = $home_rosters->find('xpath', '//table//tbody//tr[1]');
    $this->assertEquals('1', $row->find('xpath', '//td[1]')->getText());
    $this->assertEquals($roma_players[$roma_player_keys[0]]['label'], $row->find('xpath', '//td[3]')->getText());
    $row = $home_rosters->find('xpath', '//table//tbody//tr[3]');
    $this->assertEquals('3', $row->find('xpath', '//td[1]')->getText());
    $this->assertEquals($roma_players[$roma_player_keys[2]]['label'], $row->find('xpath', '//td[3]')->getText());

    // Away rosters.
    $row = $away_rosters->find('xpath', '//table//tbody//tr[1]');
    $this->assertEquals('1', $row->find('xpath', '//td[1]')->getText());
    $this->assertEquals($torino_players[$torino_player_keys[0]]['label'], $row->find('xpath', '//td[3]')->getText());
    $row = $away_rosters->find('xpath', '//table//tbody//tr[3]');
    $this->assertEquals('3', $row->find('xpath', '//td[1]')->getText());
    $this->assertEquals($torino_players[$torino_player_keys[2]]['label'], $row->find('xpath', '//td[3]')->getText());

    // Home subs.
    $row = $home_subs->find('xpath', '//table//tbody//tr[1]');
    $this->assertEquals('7', $row->find('xpath', '//td[1]')->getText());
    $this->assertEquals($roma_players[$roma_player_keys[6]]['label'], $row->find('xpath', '//td[3]')->getText());

    // Away subs.
    $row = $away_subs->find('xpath', '//table//tbody//tr[3]');
    $this->assertEquals('9', $row->find('xpath', '//td[1]')->getText());
    $this->assertEquals($torino_players[$torino_player_keys[8]]['label'], $row->find('xpath', '//td[3]')->getText());

    // Home moments.
    $row = $home_moments->find('xpath', '//table//tbody//tr[1]');
    $this->assertEquals('10', $row->find('xpath', '//td[2]')->getText());
    $this->assertEquals($roma_players[$roma_player_keys[4]]['label'], $row->find('xpath', '//td[4]')->getText());
    $row = $home_moments->find('xpath', '//table//tbody//tr[3]');
    $this->assertEquals('55', $row->find('xpath', '//td[2]')->getText());
    $this->assertEquals($roma_players[$roma_player_keys[6]]['label'] . ' replaced by ' . $roma_players[$roma_player_keys[5]]['label'], $row->find('xpath', '//td[4]')->getText());

    // Away moments.
    $row = $away_moments->find('xpath', '//table//tbody//tr[1]');
    $this->assertEquals('46', $row->find('xpath', '//td[2]')->getText());
    $this->assertEquals($torino_players[$torino_player_keys[1]]['label'], $row->find('xpath', '//td[4]')->getText());
    $row = $away_moments->find('xpath', '//table//tbody//tr[2]');
    $this->assertEquals('90', $row->find('xpath', '//td[2]')->getText());
    $this->assertEquals($torino_players[$torino_player_keys[3]]['label'] . ' replaced by ' . $torino_players[$torino_player_keys[2]]['label'], $row->find('xpath', '//td[4]')->getText());
  }

  /**
   * Adds a series of rosters.
   *
   * @param int $team
   *   The team id.
   * @param \Behat\Mink\Element\NodeElement $rosters
   *   The div element for the rosters.
   * @param int $start
   *   The starting index.
   * @param int $limit
   *   The end index.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function addRosters(int $team, NodeElement $rosters, int $start, int $limit) {
    $players = $this->slBaseHelper->findTeamPlayers($team);
    $keys = array_keys($players);

    for ($i = $start; $i < $limit; $i++) {
      $player_id = $keys[$i];
      $rosters->pressButton('Add new rosters');
      $this->assertSession()->assertWaitOnAjaxRequest();
      $rosters->findField('Player')->selectOption($players[$player_id]['label']);
      sleep(1);
      $rosters->pressButton('Create roster');
      $this->assertSession()->assertWaitOnAjaxRequest();
    }
  }

  /**
   * Adds a match moment.
   *
   * @param int $team
   *   The team id.
   * @param \Behat\Mink\Element\NodeElement $moments
   *   The div element for the rosters.
   * @param string $selector
   *   The css selector.
   * @param string $type
   *   The type of moment.
   * @param string $player
   *   The player name.
   * @param int $time
   *   The name.
   * @param int|null $player_out
   *   The player out name.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function addMatchMoment(int $team, NodeElement $moments, string $selector, string $type, string $player, int $time, ?int $player_out = NULL) {
    $players = $this->slBaseHelper->findTeamPlayers($team);
    $keys = array_keys($players);
    $player_id = $keys[$player];
    if (!empty($player_out)) {
      $player_out = $keys[$player_out];
    }

    $moments->find('css', $selector)->selectOption($type);
    $moments->pressButton('Add new match moments');
    $this->assertSession()->assertWaitOnAjaxRequest();
    if ($moments->hasField('Player')) {
      $moments->findField('Player')->selectOption($players[$player_id]['label']);
    }
    else {
      $moments->findField('In')->selectOption($players[$player_id]['label']);
      $moments->findField('Out')->selectOption($players[$player_out]['label']);
    }
    $moments->fillField('Time', $time);
    $moments->pressButton('Create match moment');
    $this->assertSession()->assertWaitOnAjaxRequest();
  }

}

<?php

namespace Drupal\Tests\sports_league\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Base test for sports league.
 */
class SportsLeagueTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'sl_default_content',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Dashboard test.
   */
  public function testSportsLeagueDashboard() {
    $user = $this->createUser();
    $this->drupalLogin($user);
    $this->drupalGet('admin/sports_league');
    $this->assertSession()->pageTextContains('Access denied');

    $admin_user = $this->createUser(['administer sl']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/sports_league');
    $this->assertSession()->pageTextNotContains('Access denied');

    $this->assertSession()->pageTextContains('There are 8 Clubs');
    $this->assertSession()->pageTextContains('There are 8 Competition Editions');
    $this->assertSession()->pageTextContains('There are 4 Competitions');
    $this->assertSession()->pageTextContains('There are 15 Matches');
    $this->assertSession()->pageTextContains('There are 152 Persons');
    $this->assertSession()->pageTextContains('There are 8 Teams');
  }

}

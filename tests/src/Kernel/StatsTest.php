<?php

declare(strict_types=1);

namespace Drupal\Tests\sports_league\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests stats computing.
 */
class StatsTest extends KernelTestBase {

  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'file',
    'image',
    'menu_ui',
    'node',
    'options',
    'sl_base',
    'sl_competition',
    'sl_club',
    'sl_default_content',
    'sl_match',
    'sl_match_moments',
    'sl_person',
    'sl_rosters',
    'sl_stats',
    'sl_team',
    'system',
    'taxonomy',
    'text',
    'user',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('file');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('node_type');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('field_storage_config');
    $this->installEntitySchema('field_config');
    $this->installEntitySchema('sl_match_moments');
    $this->installEntitySchema('sl_rosters');
    $this->installEntitySchema('sl_stats');
    $this->installSchema('file', ['file_usage']);

    $this->installConfig(['image', 'node', 'views']);
    $this->installConfig(['sl_base', 'sl_competition', 'sl_club', 'sl_team', 'taxonomy']);
    $this->installConfig(['sl_person']);
    $this->installConfig(['sl_match']);
    $this->installConfig(['sl_rosters', 'sl_match_moments']);
    $this->installConfig(['sl_stats']);

    \Drupal::moduleHandler()->loadInclude('sl_default_content', 'install');
    sl_default_content_install();
  }

  /**
   * Tests match queue is added on match save.
   */
  public function testQueue(): void {
    $queue_items = \Drupal::service('database')->select('queue')
      ->fields('queue', [
        'item_id',
      ])
      ->condition('name', 'sl_match_worker')
      ->execute()->fetchAll();

    $this->assertEmpty($queue_items);
    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties(['type' => 'sl_match']);
    // Load the first node returned from the database.
    $node = reset($nodes);
    $node->save();

    $queue_items = \Drupal::service('database')->select('queue')
      ->fields('queue', [
        'item_id',
      ])
      ->condition('name', 'sl_match_worker')
      ->execute()->fetchAll();

    $this->assertNotEmpty($queue_items);

    // Process the queue.
    $queue_factory = \Drupal::service('queue');
    $queue_manager = \Drupal::service('plugin.manager.queue_worker');
    $queue_worker = $queue_manager->createInstance('sl_match_worker');
    $queue = $queue_factory->get('sl_match_worker');
    $item = $queue->claimItem();
    $queue_worker->processItem($item->data);

    // Stats queue created.
    $stats_queue_items = \Drupal::service('database')->select('queue')
      ->fields('queue', [
        'item_id',
      ])
      ->condition('name', 'sl_stats_worker')
      ->execute()->fetchAll();

    $this->assertCount(38, $stats_queue_items);
  }

  /**
   * Tests match queue is added on match save.
   */
  public function testStatsComputer(): void {
    /** @var \Drupal\sl_stats\SLStatsComputer $stats_computer */
    $stats_computer = \Drupal::service('sl_stats.computer');
    $entity_type_manager = \Drupal::entityTypeManager();
    $stats_storage = $entity_type_manager->getStorage('sl_stats');
    $node_storage = $entity_type_manager->getStorage('node');
    $query = $node_storage->getQuery();
    $nids = $query->accessCheck(TRUE)
      ->condition('type', 'sl_person')
      ->execute();

    // Check stats were computed.
    $stats = $stats_storage->loadMultiple();
    $this->assertCount(169, $stats);

    // Check the players were created.
    $this->assertCount(152, $nids);
    foreach ($nids as $nid) {
      /** @var \Drupal\sl_stats\SLStatsComputer $stats_computer */
      $stats_computer->compute((int) $nid);
    }

    $stats = $stats_storage->loadMultiple();
    $this->assertCount(169, $stats);

    // Check coach status are correct.
    $query = $stats_storage->getQuery();
    $coach_stats_ids = $query->accessCheck(TRUE)
      ->condition('type', 'sl_stats_coach')
      ->execute();
    $this->assertCount(7, $coach_stats_ids);

    $coach_stats = $stats_storage->loadMultiple($coach_stats_ids);
    $expected_stats = [
      [66, 10, NULL, 6, 1, 0, 5, 0.17, 0 , 0.83],
      [66, 10, 27, 3, 1, 0, 2, 0.33, 0 , 0.67],
      [66, 10, 26, 2, 0, 0, 2, 0, 0, 1],
      [66, 10, 28, 1, 0, 0, 1, 0, 0 , 1],
      [180, 16, NULL, 2, 2, 0, 0, 1, 0, 0],
      [180, 16, 26, 1, 1, 0, 0, 1, 0, 0],
      [180, 16, 28, 1, 1, 0, 0, 1, 0, 0],
      [180, 16, NULL, 2, 5, 4, 2, 1, 0, 0],
    ];

    $i = 0;
    foreach ($coach_stats as $coach_stat) {
      $this->assertEquals($expected_stats[$i][0], $coach_stat->field_sl_stats_person->target_id, 'Failed in row ' . $i);
      $this->assertEquals($expected_stats[$i][1], $coach_stat->field_sl_teams->target_id, 'Failed in row ' . $i);
      $this->assertEquals($expected_stats[$i][2], $coach_stat->field_sl_stats_competition->target_id, 'Failed in row ' . $i);
      $this->assertEquals($expected_stats[$i][3], $coach_stat->field_sl_stats_matches->value, 'Failed in row ' . $i);
      $this->assertEquals($expected_stats[$i][4], $coach_stat->field_sl_stats_match_wins->value, 'Failed in row ' . $i);
      $this->assertEquals($expected_stats[$i][5], $coach_stat->field_sl_stats_match_draws->value, 'Failed in row ' . $i);
      $this->assertEquals($expected_stats[$i][6], $coach_stat->field_sl_stats_match_lost->value, 'Failed in row ' . $i);
      $this->assertEquals($expected_stats[$i][7], $coach_stat->field_sl_stats_match_perc_win->value, 'Failed in row ' . $i);
      $this->assertEquals($expected_stats[$i][8], $coach_stat->field_sl_stats_match_perc_draws->value, 'Failed in row ' . $i);
      $this->assertEquals($expected_stats[$i][9], $coach_stat->field_sl_stats_match_perc_lost->value, 'Failed in row ' . $i);
      $i++;
    }

    // Check players.
    $players_expected_stats = [
      // The keys are the node ids.
      59 => [
        [59, 10, NULL, 'sl_stats_player', 6, 540, 0, 2, 0],
        [59, 10, 26, 'sl_stats_player', 2, 180, 0, 0, 0],
        [59, 10, 27, 'sl_stats_player', 3, 270, 0, 0, 0],
        [59, 10, 28, 'sl_stats_player', 1, 90, 0, 0, 0],
        [59, 10, NULL, 'sl_stats_player_non_official', 0, 0, 0, 2, 0],
      ],
      58 => [
        [58, 10, NULL, 'sl_stats_player', 6, 540, 4, 2, 0],
        [58, 10, 26, 'sl_stats_player', 2, 180, 1, 0, 0],
        [58, 10, 27, 'sl_stats_player', 3, 270, 2, 0, 0],
        [58, 10, 28, 'sl_stats_player', 1, 90, 1, 0, 0],
        [58, 10, NULL, 'sl_stats_player_non_official', 0, 0, 0, 2, 0],
      ],
      172 => [
        [172, 16, NULL, 'sl_stats_player', 2, 180, 2, 0, 0],
        [172, 16, 26, 'sl_stats_player', 1, 90, 1, 0, 0],
        [172, 16, 28, 'sl_stats_player', 1, 90, 1, 0, 0],
        [172, 16, NULL, 'sl_stats_player_non_official', 0, 0, 0, 0, 0],
      ],
      143 => [],
    ];
    foreach ($players_expected_stats as $player_node_id => $player_stat) {
      $i = 0;
      $query = $stats_storage->getQuery();
      $player_stats_ids = $query->accessCheck(TRUE)
        ->condition('field_sl_stats_person', $player_node_id)
        ->sort('type', 'ASC')
        ->sort('field_sl_stats_competition', 'ASC')
        ->execute();
      $this->assertCount(count($players_expected_stats[$player_node_id]), $player_stats_ids);

      $player_stats = $stats_storage->loadMultiple($player_stats_ids);
      foreach ($player_stats as $player_stat) {
        $this->assertEquals($players_expected_stats[$player_node_id][$i][0], $player_stat->field_sl_stats_person->target_id, sprintf("Failed for player %s in row %s", $player_node_id, $i));
        $this->assertEquals($players_expected_stats[$player_node_id][$i][1], $player_stat->field_sl_teams->target_id, sprintf("Failed for player %s in row %s", $player_node_id, $i));
        $this->assertEquals($players_expected_stats[$player_node_id][$i][2], $player_stat->field_sl_stats_competition->target_id ?? NULL, sprintf("Failed for player %s in row %s", $player_node_id, $i));
        $this->assertEquals($players_expected_stats[$player_node_id][$i][3], $player_stat->bundle(), sprintf("Failed for player %s in row %s", $player_node_id, $i));
        $this->assertEquals($players_expected_stats[$player_node_id][$i][4], $player_stat->field_sl_stats_matches->value, sprintf("Failed for player %s in row %s", $player_node_id, $i));
        $this->assertEquals($players_expected_stats[$player_node_id][$i][5], $player_stat->field_sl_stats_minutes->value, sprintf("Failed for player %s in row %s", $player_node_id, $i));
        $this->assertEquals($players_expected_stats[$player_node_id][$i][6], $player_stat->field_sl_stats_goals->value, sprintf("Failed for player %s in row %s", $player_node_id, $i));
        $this->assertEquals($players_expected_stats[$player_node_id][$i][7], $player_stat->field_sl_stats_unofficial_matche->value, sprintf("Failed for player %s in row %s", $player_node_id, $i));
        $this->assertEquals($players_expected_stats[$player_node_id][$i][8], $player_stat->field_sl_stats_unofficial_goals->value, sprintf("Failed for player %s in row %s", $player_node_id, $i));
        $i++;
      }
    }
  }

}

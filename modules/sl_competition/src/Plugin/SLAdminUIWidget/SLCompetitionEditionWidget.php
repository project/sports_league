<?php

namespace Drupal\sl_competition\Plugin\SLAdminUIWidget;

use Drupal\sl_admin_ui\SLAdminUIWidgetBase;

/**
 * Provides a basic widget.
 *
 * @SLAdminUIWidget(
 *   id = "sl_competition_edition",
 *   name = @Translation("Competition Editions"),
 *   description = @Translation("Competitions editions have matches/standings"),
 *   bundle = "sl_competition_edition"
 * )
 */
class SLCompetitionEditionWidget extends SLAdminUIWidgetBase {

}

<?php

namespace Drupal\sl_competition\Plugin\SLAdminUIWidget;

use Drupal\sl_admin_ui\SLAdminUIWidgetBase;

/**
 * Provides a basic widget.
 *
 * @SLAdminUIWidget(
 *   id = "sl_competition",
 *   name = @Translation("Competitions"),
 *   description = @Translation("Competitions have competition editions"),
 *   bundle = "sl_competition"
 * )
 */
class SLCompetitionWidget extends SLAdminUIWidgetBase {
}

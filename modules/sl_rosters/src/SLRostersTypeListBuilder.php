<?php

declare(strict_types=1);

namespace Drupal\sl_rosters;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of sports league rosters type entities.
 *
 * @see \Drupal\sl_rosters\Entity\SLRostersType
 */
final class SLRostersTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No sports league rosters types available. <a href=":link">Add sports league rosters type</a>.',
      [':link' => Url::fromRoute('entity.sl_rosters_type.add_form')->toString()],
    );

    return $build;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\sl_rosters\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sl_rosters\Entity\SLRostersType;

/**
 * Form controller for the sports league rosters entity edit forms.
 */
final class SLRostersForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\sl_rosters\Entity\SLRosters $entity */
    $entity = $this->entity;
    $bundle = SLRostersType::load($entity->bundle());
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('<em>Edit @type</em> @title', [
        '@type' => empty($bundle) ? $entity->bundle() : $bundle->label(),
        '@title' => $entity->label(),
      ]);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New sports league rosters %label has been created.', $message_args));
        $this->logger('sl_rosters')->notice('New sports league rosters %label has been created.', $logger_args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The sports league rosters %label has been updated.', $message_args));
        $this->logger('sl_rosters')->notice('The sports league rosters %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $result;
  }

}

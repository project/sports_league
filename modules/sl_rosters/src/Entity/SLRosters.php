<?php

declare(strict_types=1);

namespace Drupal\sl_rosters\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the sports league rosters entity class.
 *
 * @ContentEntityType(
 *   id = "sl_rosters",
 *   label = @Translation("Rosters"),
 *   label_collection = @Translation("Rosters"),
 *   label_singular = @Translation("rosters"),
 *   label_plural = @Translation("rosters"),
 *   label_count = @PluralTranslation(
 *     singular = "@count rosters",
 *     plural = "@count rosters",
 *   ),
 *   bundle_label = @Translation("Type"),
 *   handlers = {
 *     "list_builder" = "Drupal\sl_rosters\SLRostersListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\sl_rosters\SLRostersAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\sl_rosters\Form\SLRostersForm",
 *       "edit" = "Drupal\sl_rosters\Form\SLRostersForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\sl_base\Routing\SLHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "sl_rosters",
 *   data_table = "sl_rosters_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer sl_rosters types",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "bundle" = "type",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/sl-rosters",
 *     "add-form" = "/sl-rosters/add/{sl_rosters_type}",
 *     "add-page" = "/sl-rosters/add",
 *     "canonical" = "/sl-rosters/{sl_rosters}",
 *     "edit-form" = "/sl-rosters/{sl_rosters}",
 *     "delete-form" = "/sl-rosters/{sl_rosters}/delete",
 *     "delete-multiple-form" = "/admin/content/sl-rosters/delete-multiple",
 *   },
 *   bundle_entity_type = "sl_rosters_type",
 *   field_ui_base_route = "entity.sl_rosters_type.edit_form",
 * )
 */
final class SLRosters extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public function label() {
    if (!empty($this->field_sl_roster_player_name)) {
      return $this->field_sl_roster_player_name->value;
    }
    elseif (!is_null($this->field_sl_roster_player->entity)) {
      return $this->field_sl_roster_player->entity->label();
    }
  }

}

<?php

declare(strict_types=1);

namespace Drupal\sl_rosters\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Sports League Rosters type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "sl_rosters_type",
 *   label = @Translation("Sports League Rosters type"),
 *   label_collection = @Translation("Sports League Rosters types"),
 *   label_singular = @Translation("sports league rosters type"),
 *   label_plural = @Translation("sports league rosterss types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count sports league rosterss type",
 *     plural = "@count sports league rosterss types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\sl_rosters\Form\SLRostersTypeForm",
 *       "edit" = "Drupal\sl_rosters\Form\SLRostersTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\sl_rosters\SLRostersTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer sl_rosters types",
 *   bundle_of = "sl_rosters",
 *   config_prefix = "sl_rosters_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/sl_rosters_types/add",
 *     "edit-form" = "/admin/structure/sl_rosters_types/manage/{sl_rosters_type}",
 *     "delete-form" = "/admin/structure/sl_rosters_types/manage/{sl_rosters_type}/delete",
 *     "collection" = "/admin/structure/sl_rosters_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class SLRostersType extends ConfigEntityBundleBase {

  /**
   * The machine name of this sports league rosters type.
   */
  protected string $id;

  /**
   * The human-readable name of the sports league rosters type.
   */
  protected string $label;

}

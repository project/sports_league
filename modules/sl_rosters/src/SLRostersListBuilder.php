<?php

declare(strict_types=1);

namespace Drupal\sl_rosters;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\sl_rosters\Entity\SLRostersType;

/**
 * Provides a list controller for the sports league rosters entity type.
 */
final class SLRostersListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['match'] = $this->t('Match');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\sl_rosters\SLRostersInterface $entity */
    $row['id'] = $entity->id();
    $bundle = SLRostersType::load($entity->bundle());
    $row['type'] = !(empty($bundle)) ? $bundle->label() : $entity->bundle();
    $row['match'] = '';
    if (!empty($entity->field_sl_match->entity)) {
      $row['match'] = $entity->field_sl_match->entity->toLink();
    }

    return $row + parent::buildRow($entity);
  }

}

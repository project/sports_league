<?php

namespace Drupal\sl_person\Plugin\SLAdminUIWidget;

use Drupal\sl_admin_ui\SLAdminUIWidgetBase;

/**
 * Provides a basic widget.
 *
 * @SLAdminUIWidget(
 *   id = "sl_person",
 *   name = @Translation("Persons"),
 *   description = @Translation("Persons are players and coaches"),
 *   bundle = "sl_person"
 * )
 */
class SLPersonWidget extends SLAdminUIWidgetBase {


}

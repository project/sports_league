<?php

namespace Drupal\sl_match\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\sl_match\SLMatchWorker;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manages stats in a queue.
 *
 * @QueueWorker(
 *   id = "sl_match_worker",
 *   title = @Translation("SL Match worker"),
 * )
 */
class SLMatchQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The match worker.
   *
   * @var \Drupal\sl_match\SLMatchWorker
   */
  protected $slMatchWorker;

  /**
   * SLMatchQueueWorker constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\sl_match\SLMatchWorker $sl_match_worker
   *   The match worker.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, SLMatchWorker $sl_match_worker) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->slMatchWorker = $sl_match_worker;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('sl_match.worker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $status = $this->slMatchWorker->compute($data->nid);
    return $status;
  }

}

<?php

namespace Drupal\sl_match\Plugin\SLAdminUIWidget;

use Drupal\sl_admin_ui\SLAdminUIWidgetBase;

/**
 * Provides a basic widget.
 *
 * @SLAdminUIWidget(
 *   id = "sl_match",
 *   name = @Translation("Matches"),
 *   description = @Translation("Matches are between teams"),
 *   bundle = "sl_match"
 * )
 */
class SLMatchWidget extends SLAdminUIWidgetBase {


}

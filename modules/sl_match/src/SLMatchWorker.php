<?php

namespace Drupal\sl_match;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;

/**
 * The match worker service.
 */
class SLMatchWorker {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SLMatchWorker constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Computes the match.
   */
  public function compute($match_id) {

    if (!is_numeric($match_id)) {
      return;
    }

    $node_manager = $this->entityTypeManager->getStorage('node');
    $moments_manager = $this->entityTypeManager->getStorage('sl_match_moments');
    $rosters_manager = $this->entityTypeManager->getStorage('sl_rosters');

    $node = $node_manager->load($match_id);
    if (empty($node)) {
      return;
    }

    $efq = $moments_manager->getQuery();
    $efq->condition('field_sl_match', $match_id);
    $efq->accessCheck(TRUE);
    $moments_ids = $efq->execute();

    $away_starters = array_column($node->field_sl_match_away_inirosters->getValue(), 'target_id');
    $away_coach = array_column($node->field_sl_match_away_coach->getValue(), 'target_id');
    $home_starters = array_column($node->field_sl_match_home_inirosters->getValue(), 'target_id');
    $home_coach = array_column($node->field_sl_match_home_coach->getValue(), 'target_id');

    $counting = array_merge_recursive($away_starters, $away_coach, $home_starters, $home_coach);

    $in = $out = [];
    if (!empty($moments_ids)) {
      $moments = $moments_manager->loadMultiple($moments_ids);
      foreach ($moments as $mom) {
        $mom_type = $mom->bundle();
        if ($mom_type == 'sl_match_moments_substitution' && !empty($mom->field_sl_match_moments_player->target_id) && !empty($mom->field_sl_match_moments_player_in->target_id)) {
          $out[$mom->field_sl_match_moments_player->target_id] = $mom->field_sl_match_moments_time->value;
          $in[$mom->field_sl_match_moments_player_in->target_id] = $mom->field_sl_match_moments_time->value;
        }
      }
    }

    $efq = $rosters_manager->getQuery();
    $efq->condition('type', ['sl_rosters', 'sl_rosters_coach'], 'IN');
    $efq->condition('field_sl_match', $match_id);
    $efq->accessCheck(TRUE);
    $rosters_ids = $efq->execute();

    $rosters = $rosters_manager->loadMultiple($rosters_ids);

    foreach ($rosters as $roster) {
      if (in_array($roster->field_sl_roster_player->target_id, array_keys($out))) {
        $roster->set('field_sl_roster_out', $out[$roster->field_sl_roster_player->target_id]);
        $roster->set('field_sl_count', TRUE);
        $roster->save();
      }
      elseif (in_array($roster->field_sl_roster_player->target_id, array_keys($in))) {
        $roster->set('field_sl_roster_in', $in[$roster->field_sl_roster_player->target_id]);
        $roster->set('field_sl_roster_out', 90);
        $roster->set('field_sl_count', TRUE);
        $roster->save();
      }
      elseif (in_array($roster->id(), $counting)) {
        // Only set official the full rosters.
        $roster->set('field_sl_count', TRUE);
        $roster->save();
      }

      // Compute all players.
      if (is_numeric($roster->field_sl_roster_player->target_id)) {
        $player = $node_manager->load($roster->field_sl_roster_player->target_id);
        if (!empty($player)) {
          $player->save();
        }
      }
    }

    return $node;
  }

  /**
   * Gets the score.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The match.
   *
   * @return string
   *   The score.
   */
  public function getScore(NodeInterface $node): string {
    if (!empty($node->field_sl_goals_home_penalties->value) && !empty($node->field_sl_goals_away_penalties->value)) {
      return $node->field_sl_match_score_home->value . ' - ' . $node->field_sl_match_score_away->value . ' ' . t('(:home_penalties x :away_penalties p.s.)', [
        ':home_penalties' => $node->field_sl_goals_home_penalties->value,
        ':away_penalties' => $node->field_sl_goals_away_penalties->value,
      ]);
    }
    else {
      return $node->field_sl_match_score_home->value . ' - ' . $node->field_sl_match_score_away->value;
    }
  }

}

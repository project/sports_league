<?php

namespace Drupal\sl_base;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Helper for Sports League.
 */
class SlBaseHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * SlBaseHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cache backend service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CacheBackendInterface $cacheBackend) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cacheBackend = $cacheBackend;
  }

  /**
   * Finds team players.
   *
   * @param int $team
   *   The team.
   *
   * @return array
   *   The team players.
   */
  public function findTeamPlayers(int $team): array {
    $player_options = [];
    $cached_info = $this->cacheBackend->get('sb_base_team_players_' . $team);
    if (empty($cached_info)) {
      $efq = $this->entityTypeManager->getStorage('node')->getQuery();
      $efq->condition('type', 'sl_person')
        ->condition('status', 1, '=')
        ->accessCheck(FALSE)
        ->condition('field_sl_teams', $team);
      $efq->accessCheck(TRUE);
      $result = $efq->execute();
      $players = $this->entityTypeManager->getStorage('node')->loadMultiple($result);
      $this->cacheBackend->set('sb_base_team_players_' . $team, $players, time() + (60 * 60), ['node_list']);
    }
    else {
      $players = $cached_info->data;
    }

    foreach ($players as $player) {
      $player_options[$player->id()] = ['label' => $player->label(), 'number' => $player->field_sl_person_number->value];
    }

    return $player_options;
  }

}

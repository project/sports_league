<?php

declare(strict_types=1);

namespace Drupal\sl_transfer\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the sports league transfers entity class.
 *
 * @ContentEntityType(
 *   id = "sl_transfers",
 *   label = @Translation("Transfers"),
 *   label_collection = @Translation("transfers"),
 *   label_singular = @Translation("transfers"),
 *   label_plural = @Translation("transfers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count transfers",
 *     plural = "@count transfers",
 *   ),
 *   bundle_label = @Translation("Sports League transfers type"),
 *   handlers = {
 *     "list_builder" = "Drupal\sl_transfer\SLTransfersListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\sl_transfer\SLTransfersAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\sl_transfer\Form\SLTransfersForm",
 *       "edit" = "Drupal\sl_transfer\Form\SLTransfersForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\sl_base\Routing\SLHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "sl_transfers",
 *   data_table = "sl_transfers_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer sl",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/sl-transfers",
 *     "add-form" = "/sl-transfers/add/{sl_transfers_type}",
 *     "add-page" = "/sl-transfers/add",
 *     "canonical" = "/sl-transfers/{sl_transfers}",
 *     "edit-form" = "/sl-transfers/{sl_transfers}",
 *     "delete-form" = "/sl-transfers/{sl_transfers}/delete",
 *     "delete-multiple-form" = "/admin/content/sl-transfers/delete-multiple",
 *   },
 *   bundle_entity_type = "sl_transfers_type",
 *   field_ui_base_route = "entity.sl_transfers_type.edit_form",
 * )
 */
final class SLTransfers extends ContentEntityBase {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(FALSE)
      ->setDescription(t('The time that the sports league transfers was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(FALSE)
      ->setDescription(t('The time that the sports league transfers was last edited.'));

    return $fields;
  }

}

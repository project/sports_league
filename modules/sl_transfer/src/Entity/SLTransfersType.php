<?php

declare(strict_types=1);

namespace Drupal\sl_transfer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Sports League transfers type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "sl_transfers_type",
 *   label = @Translation("Sports League transfers type"),
 *   label_collection = @Translation("Sports League transfers types"),
 *   label_singular = @Translation("sports league transfers type"),
 *   label_plural = @Translation("sports league transfers types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count sports league transfers type",
 *     plural = "@count sports league transfers types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\sl_transfer\Form\SLTransfersTypeForm",
 *       "edit" = "Drupal\sl_transfer\Form\SLTransfersTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\sl_transfer\SLTransfersTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer sl",
 *   bundle_of = "sl_transfers",
 *   config_prefix = "sl_transfers_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/sl_transfers_types/add",
 *     "edit-form" = "/admin/structure/sl_transfers_types/manage/{sl_transfers_type}",
 *     "delete-form" = "/admin/structure/sl_transfers_types/manage/{sl_transfers_type}/delete",
 *     "collection" = "/admin/structure/sl_transfers_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class SLTransfersType extends ConfigEntityBundleBase {

  /**
   * The machine name of this sports league transfers type.
   */
  protected string $id;

  /**
   * The human-readable name of the sports league transfers type.
   */
  protected string $label;

}

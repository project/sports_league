<?php

declare(strict_types=1);

namespace Drupal\sl_transfer;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the sports league transfers entity type.
 */
final class SLTransfersListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['player'] = $this->t('Player');
    $header['type'] = $this->t('Type');
    $header['date'] = $this->t('Date');
    $header['uid'] = $this->t('Author');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\sl_transfers\SLTransfersInterface $entity */
    $row['id'] = $entity->id();
    $row['player']['data'] = '';
    if (!empty($entity->field_sl_transfer_player->entity)) {
      $row['player']['data'] = $entity->field_sl_transfer_player->entity->toLink();
    }

    $row['type'] = $entity->field_sl_transfer_type->value;
    $row['date'] = date('Y-m-d', (int) $entity->field_sl_transfer_date->value);

    $username_options = [
      'label' => 'hidden',
      'settings' => ['link' => $entity->get('uid')->entity->isAuthenticated()],
    ];
    $row['uid']['data'] = $entity->get('uid')->view($username_options);
    $row['created']['data'] = $entity->get('created')->view(['label' => 'hidden']);
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    return $row + parent::buildRow($entity);
  }

}

<?php

declare(strict_types=1);

namespace Drupal\sl_transfer;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of sports league transfers type entities.
 *
 * @see \Drupal\sl_transfer\Entity\SLTransfersType
 */
final class SLTransfersTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No sports league transfers types available. <a href=":link">Add sports league transfers type</a>.',
      [':link' => Url::fromRoute('entity.sl_transfers_type.add_form')->toString()],
    );

    return $build;
  }

}

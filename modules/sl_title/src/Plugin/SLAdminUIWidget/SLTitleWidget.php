<?php

namespace Drupal\sl_title\Plugin\SLAdminUIWidget;

use Drupal\sl_admin_ui\SLAdminUIWidgetBase;

/**
 * Provides a basic widget.
 *
 * @SLAdminUIWidget(
 *   id = "sl_title",
 *   name = @Translation("Titles"),
 *   description = @Translation("Titles are wons by teams in competition editions"),
 *   bundle = "sl_title"
 * )
 */
class SLTitleWidget extends SLAdminUIWidgetBase {


}

<?php

namespace Drupal\sl_admin_ui;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base widget for sl admin ui dasahboard.
 */
class SLAdminUIWidgetBase extends PluginBase implements SLAdminUIWidgetPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->pluginDefinition['name'];
  }

  /**
   * Renders the content table.
   */
  public function contentTable() {
  }

  /**
   * Renders the content.
   */
  public function content() {

    $ids = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $this->pluginDefinition['bundle']);

    if ($this->pluginDefinition['bundle'] == 'sl_competition_edition') {
      $ids->condition('field_sl_archived', 0);
    }

    $result = $ids->execute();

    return [
      'title' => $this->pluginDefinition['name'],
      'description' => $this->pluginDefinition['description'],
      'current' => $this->formatPlural(count($result), 'There is %items item', 'There are %items %items_label',
        [
          '%items' => count($result),
          '%items_label' => $this->pluginDefinition['name'],
        ]),
      'links' => [
        '#theme' => 'item_list',
        '#items' => [
          Link::fromTextAndUrl('Add new', Url::fromRoute('node.add', ['node_type' => $this->pluginDefinition['bundle']])),
          Link::fromTextAndUrl('View all', $url = Url::fromUri('internal:/admin/sports_league/sl-admin-ui-content/' . $this->pluginDefinition['bundle'])),
        ],
      ],
      'content' => $this->contentTable(),
    ];
  }

}

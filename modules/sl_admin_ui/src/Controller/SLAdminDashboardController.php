<?php

namespace Drupal\sl_admin_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sl_admin_ui\SLAdminUIWidgetsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The dashboard controller.
 */
class SLAdminDashboardController extends ControllerBase {

  /**
   * The plugin manager.
   *
   * @var \Drupal\sl_admin_ui\SLAdminUIWidgetsManager
   */
  protected $pluginManager;

  /**
   * SLAdminDashboardController constructor.
   *
   * @param \Drupal\sl_admin_ui\SLAdminUIWidgetsManager $sl_admin_ui_widgets_manager
   *   The plugin manager.
   */
  public function __construct(SLAdminUIWidgetsManager $sl_admin_ui_widgets_manager) {
    $this->pluginManager = $sl_admin_ui_widgets_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.sl_admin_ui_widgets')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function content() {
    $form_plugins = $this->pluginManager->getDefinitions();
    $widgets = [];
    foreach ($form_plugins as $name => $plugin) {
      $widgets[$name] = $this->pluginManager->createInstance($name)->content();
    }

    $build = [
      '#theme' => 'sl_admin_ui_dashboard',
      '#widgets' => $widgets,
    ];
    return $build;
  }

}

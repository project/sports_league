<?php

namespace Drupal\sl_admin_ui\Plugin\EntityBrowser\FieldWidgetDisplay;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_browser\Plugin\EntityBrowser\FieldWidgetDisplay\EntityLabel;

/**
 * Displays a label of the entity.
 *
 * @EntityBrowserFieldWidgetDisplay(
 *   id = "sl_admin_label",
 *   label = @Translation("Administrative Entity label"),
 *   description = @Translation("Displays administrative entity with a label.")
 * )
 */
class AdministrativeEntityLabel extends EntityLabel {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity) {
    $translation = $this->entityRepository->getTranslationFromContext($entity);

    if (!$translation->access('view label')) {
      $restricted_access_label = new FormattableMarkup('@label @id', [
        '@label' => $entity->getEntityType()->getSingularLabel(),
        '@id' => $entity->id(),
      ]);
      return ['#markup' => $restricted_access_label];
    }

    return ['#markup' => $translation->id() . ' - ' . $translation->field_sl_administrative_title->value];
  }

}

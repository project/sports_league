/**
 Change numbers on rosters
 */
Drupal.behaviors.selectNumberPlayer = {
  attach(context, settings) {
    jQuery('.sl_roster_player_selection').change(function () {
      const rand = jQuery(this).attr('data-attribute-number');
      const selected = this.value;
      const selects = document.querySelectorAll(
        `.sl_roster_player_number_${rand}`,
      );
      selects.forEach(function (input) {
        input.value = drupalSettings.sl_rosters[selected];
      });
    });
  },
};

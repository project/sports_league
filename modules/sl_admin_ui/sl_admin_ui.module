<?php

/**
 * @file
 * The SL admin ui module.
 */

declare(strict_types=1);

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function sl_admin_ui_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the sl_admin_ui module.
    case 'help.page.sl_admin_ui':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Sports League Admin UI') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function sl_admin_ui_theme($existing, $type, $theme, $path) {
  return [
    'sl_admin_ui_dashboard' => [
      'variables' => ['widgets' => NULL],
    ],
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sl_admin_ui_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  $entity = $form_state->getFormObject()->getEntity();

  // Disable all administrative titles.
  if (isset($form['field_sl_administrative_title'])) {
    $form['field_sl_administrative_title']['widget'][0]['value']['#disabled'] = TRUE;
  }

  // For matches.
  if ($entity->bundle() == 'sl_match' && !($entity->id())) {
    unset($form['field_sl_match_home_coach']);
    unset($form['field_sl_match_away_coach']);
    unset($form['field_sl_match_away_inirosters']);
    unset($form['field_sl_match_home_inirosters']);
    unset($form['field_sl_match_away_moments']);
    unset($form['field_sl_match_home_moments']);
    unset($form['field_sl_match_away_inisubs']);
    unset($form['field_sl_match_home_inisubs']);
  }

  // Add correct library.
  $form['#attached']['library'][] = 'sl_admin_ui/sl_admin_ui';
}

/**
 * Implements hook_entity_type_presave().
 */
function sl_admin_ui_node_presave(EntityInterface $entity) {

  $active_types = ['sl_competition', 'sl_competition_edition', 'sl_team', 'sl_club', 'sl_title'];
  if ($entity->getEntityTypeId() == 'node' && in_array($entity->bundle(), $active_types)) {
    $taxonomy_manager = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term');
    $administrative_title = '';

    if (!empty($entity->field_sl_categories->target_id)) {
      $parents = $taxonomy_manager->loadParents($entity->field_sl_categories->target_id);
      $term_names = [];

      foreach ($parents as $term) {
        $term_names[] = $term->label();
      }

      $term_names[] = $entity->field_sl_categories->entity->label();
      $terms = implode(" > ", $term_names);
      if (!empty($terms)) {
        $administrative_title = $terms . ' > ';
      }
    }

    $entity->set('field_sl_administrative_title', $administrative_title . $entity->title->value);
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function sl_admin_ui_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'views_data_alter') {
    // Fix problem with shs in exposed views.
    if (!empty($implementations['shs'])) {
      $group = $implementations['shs'];
      unset($implementations['shs']);
      $implementations['shs'] = $group;
    }
  }
}

/**
 * Implements hook_toolbar_alter().
 */
function sl_admin_ui_toolbar_alter(&$items) {
  $items['administration']['#attached']['library'][] = 'sl_admin_ui/sl_admin_ui';
}

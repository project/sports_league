<?php

declare(strict_types=1);

namespace Drupal\sl_match_moments\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Sports League Match Moments type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "sl_match_moments_type",
 *   label = @Translation("Sports League Match Moments type"),
 *   label_collection = @Translation("Sports League Match Moments types"),
 *   label_singular = @Translation("sports league match moments type"),
 *   label_plural = @Translation("sports league match moments types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count sports league match moments type",
 *     plural = "@count sports league match moments types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\sl_match_moments\Form\SLMatchMomentsTypeForm",
 *       "edit" = "Drupal\sl_match_moments\Form\SLMatchMomentsTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\sl_match_moments\SLMatchMomentsTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\sl_base\BundleAccessControlHandler",
 *   },
 *   admin_permission = "administer sl_match_moments types",
 *   bundle_of = "sl_match_moments",
 *   config_prefix = "sl_match_moments_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/sl_match_moments_types/add",
 *     "edit-form" = "/admin/structure/sl_match_moments_types/manage/{sl_match_moments_type}",
 *     "delete-form" = "/admin/structure/sl_match_moments_types/manage/{sl_match_moments_type}/delete",
 *     "collection" = "/admin/structure/sl_match_moments_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class SLMatchMomentsType extends ConfigEntityBundleBase {

  /**
   * The machine name of this sports league match moments type.
   */
  protected string $id;

  /**
   * The human-readable name of the sports league match moments type.
   */
  protected string $label;

}

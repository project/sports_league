<?php

declare(strict_types=1);

namespace Drupal\sl_match_moments\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the sports league match moments entity class.
 *
 * @ContentEntityType(
 *   id = "sl_match_moments",
 *   label = @Translation("Match Moments"),
 *   label_collection = @Translation("Match Moments"),
 *   label_singular = @Translation("match moments"),
 *   label_plural = @Translation("match moments"),
 *   label_count = @PluralTranslation(
 *     singular = "@count match moments",
 *     plural = "@count match moments",
 *   ),
 *   bundle_label = @Translation("Type"),
 *   handlers = {
 *     "list_builder" = "Drupal\sl_match_moments\SLMatchMomentsListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\sl_match_moments\SLMatchMomentsAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\sl_match_moments\Form\SLMatchMomentsForm",
 *       "edit" = "Drupal\sl_match_moments\Form\SLMatchMomentsForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\sl_base\Routing\SLHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "sl_match_moments",
 *   data_table = "sl_match_moments_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer sl",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "bundle" = "type",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/sl-match-moments",
 *     "add-form" = "/sl-match-moments/add/{sl_match_moments_type}",
 *     "add-page" = "/sl-match-moments/add",
 *     "canonical" = "/sl-match-moments/{sl_match_moments}",
 *     "edit-form" = "/sl-match-moments/{sl_match_moments}",
 *     "delete-form" = "/sl-match-moments/{sl_match_moments}/delete",
 *     "delete-multiple-form" = "/admin/content/sl-match-moments/delete-multiple",
 *   },
 *   bundle_entity_type = "sl_match_moments_type",
 *   field_ui_base_route = "entity.sl_match_moments_type.edit_form",
 * )
 */
final class SLMatchMoments extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public function label() {

    if (!is_null($this->field_sl_match_moments_player->entity)) {
      $label = $this->field_sl_match_moments_player->entity->label();
      return $label;
    }
    elseif (!empty($this->field_sl_match_moments_des)) {
      return $this->field_sl_match_moments_des->value;
    }
  }

}

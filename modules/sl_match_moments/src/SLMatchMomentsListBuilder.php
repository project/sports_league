<?php

declare(strict_types=1);

namespace Drupal\sl_match_moments;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\sl_match_moments\Entity\SLMatchMomentsType;

/**
 * Provides a list controller for the sports league match moments entity type.
 */
final class SLMatchMomentsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['match'] = $this->t('Match');
    $header['player'] = $this->t('Player');
    $header['minute'] = $this->t('Minute');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\sl_match_moments\SLMatchMomentsInterface $entity */
    $row['id'] = $entity->id();
    $bundle = SLMatchMomentsType::load($entity->bundle());
    $row['type'] = !empty($bundle) ? $bundle->label() : $entity->bundle();
    if (!empty($entity->field_sl_match->entity)) {
      $row['match'] = $entity->field_sl_match->entity->toLink();
    }
    $row['player'] = '';
    if (!empty($entity->field_sl_match_moments_player->entity)) {
      $row['player'] = $entity->field_sl_match_moments_player->entity->toLink();
    }
    $row['minute'] = $entity->field_sl_match_moments_time->value ?? '';
    return $row + parent::buildRow($entity);
  }

}

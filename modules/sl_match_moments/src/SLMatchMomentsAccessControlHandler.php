<?php

declare(strict_types=1);

namespace Drupal\sl_match_moments;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for the SL match moments entity type.
 */
final class SLMatchMomentsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    if ($account->hasPermission($this->entityType->getAdminPermission())) {
      return AccessResult::allowed()->cachePerPermissions();
    }

    return match($operation) {
      'view' => AccessResult::allowedIfHasPermission($account, 'view sl_match_moments'),
      'update' => AccessResult::allowedIfHasPermission($account, 'edit sl_match_moments'),
      'delete' => AccessResult::allowedIfHasPermission($account, 'delete sl_match_moments'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, [
      'create sl_match_moments',
      'administer sl',
    ], 'OR');
  }

}

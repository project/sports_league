<?php

declare(strict_types=1);

namespace Drupal\sl_match_moments;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * List builder for sl match moments type entities.
 *
 * @see \Drupal\sl_match_moments\Entity\SLMatchMomentsType
 */
final class SLMatchMomentsTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No sports league match moments types available. <a href=":link">Add sports league match moments type</a>.',
      [':link' => Url::fromRoute('entity.sl_match_moments_type.add_form')->toString()],
    );

    return $build;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\sl_match_moments\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sl_match_moments\Entity\SLMatchMomentsType;

/**
 * Form controller for the sports league match moments entity edit forms.
 */
final class SLMatchMomentsForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\sl_match_moments\Entity\SLMatchMoments $entity */
    $entity = $this->entity;
    $bundle = SLMatchMomentsType::load($entity->bundle());
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('<em>Edit @type</em> @title', [
        '@type' => empty($bundle) ? $entity->bundle() : $bundle->label(),
        '@title' => $entity->label(),
      ]);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New sports league match moments %label has been created.', $message_args));
        $this->logger('sl_match_moments')->notice('New sports league match moments %label has been created.', $logger_args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The sports league match moments %label has been updated.', $message_args));
        $this->logger('sl_match_moments')->notice('The sports league match moments %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $result;
  }

}

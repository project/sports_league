<?php

declare(strict_types=1);

namespace Drupal\sl_standings;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the sl standings rosters entity type.
 */
final class SLStandingsRostersListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['standings'] = $this->t('Standings');
    $header['team'] = $this->t('Team');
    $header['position'] = $this->t('Position');
    $header['status'] = $this->t('Status');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\sl_standings_rosters\SLStandingsRostersInterface $entity */
    $row['id'] = $entity->id();

    if (!empty($entity->field_sl_standings_reference->entity)) {
      $row['standings']['data'] = $entity->field_sl_standings_reference->entity->toLink();
    }

    if (!empty($entity->field_sl_standings_rosters_team->entity)) {
      $row['team']['data'] = $entity->field_sl_standings_rosters_team->entity->toLink();
    }
    else {
      $row['team']['data'] = '';
    }

    $row['position']['data'] = $entity->field_sl_standings_rosters_pos->value;

    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $row['created']['data'] = $entity->get('created')->view(['label' => 'hidden']);
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    return $row + parent::buildRow($entity);
  }

}

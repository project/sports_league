<?php

declare(strict_types=1);

namespace Drupal\sl_standings\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the sl standings rosters entity class.
 *
 * @ContentEntityType(
 *   id = "sl_standings_rosters",
 *   label = @Translation("Standings rosters"),
 *   label_collection = @Translation("Standings rosters"),
 *   label_singular = @Translation("standings rosters"),
 *   label_plural = @Translation("standings rosters"),
 *   label_count = @PluralTranslation(
 *     singular = "@count standings rosters",
 *     plural = "@count standings rosters",
 *   ),
 *   bundle_label = @Translation("SL Standings rosters type"),
 *   handlers = {
 *     "list_builder" = "Drupal\sl_standings\SLStandingsRostersListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\sl_standings\SLStandingsRostersAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\sl_standings\Form\SLStandingsRostersForm",
 *       "edit" = "Drupal\sl_standings\Form\SLStandingsRostersForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\sl_base\Routing\SLHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "sl_standings_rosters",
 *   data_table = "sl_standings_rosters_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer sl",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/sl-standings-rosters",
 *     "add-form" = "/sl-standings-rosters/add/{sl_standings_rosters_type}",
 *     "add-page" = "/sl-standings-rosters/add",
 *     "canonical" = "/sl-standings-rosters/{sl_standings_rosters}",
 *     "edit-form" = "/sl-standings-rosters/{sl_standings_rosters}",
 *     "delete-form" = "/sl-standings-rosters/{sl_standings_rosters}/delete",
 *     "delete-multiple-form" = "/admin/content/sl-standings-rosters/delete-multiple",
 *   },
 *   bundle_entity_type = "sl_standings_rosters_type",
 *   field_ui_base_route = "entity.sl_standings_rosters_type.edit_form",
 * )
 */
final class SLStandingsRosters extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the sl standings rosters was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the sl standings rosters was last edited.'));

    return $fields;
  }

}

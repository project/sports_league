<?php

declare(strict_types=1);

namespace Drupal\sl_standings\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the SL Standings rosters type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "sl_standings_rosters_type",
 *   label = @Translation("Sports League Standings rosters type"),
 *   label_collection = @Translation("Sports League Standings rosters types"),
 *   label_singular = @Translation("sl standings rosters type"),
 *   label_plural = @Translation("sl standings rosters types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count sl standings rosters type",
 *     plural = "@count sl standings rosters types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\sl_standings\Form\SLStandingsRostersTypeForm",
 *       "edit" = "Drupal\sl_standings\Form\SLStandingsRostersTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\sl_standings\SLStandingsRostersTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer sl",
 *   bundle_of = "sl_standings_rosters",
 *   config_prefix = "sl_standings_rosters_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/sl_standings_rosters_types/add",
 *     "edit-form" = "/admin/structure/sl_standings_rosters_types/manage/{sl_standings_rosters_type}",
 *     "delete-form" = "/admin/structure/sl_standings_rosters_types/manage/{sl_standings_rosters_type}/delete",
 *     "collection" = "/admin/structure/sl_standings_rosters_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class SLStandingsRostersType extends ConfigEntityBundleBase {

  /**
   * The machine name of this sl standings rosters type.
   */
  protected string $id;

  /**
   * The human-readable name of the sl standings rosters type.
   */
  protected string $label;

}

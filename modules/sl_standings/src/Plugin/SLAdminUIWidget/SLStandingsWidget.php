<?php

namespace Drupal\sl_standings\Plugin\SLAdminUIWidget;

use Drupal\sl_admin_ui\SLAdminUIWidgetBase;

/**
 * Provides a basic widget.
 *
 * @SLAdminUIWidget(
 *   id = "sl_standings",
 *   name = @Translation("Standings"),
 *   description = @Translation("Standings rank teams in competition editions"),
 *   bundle = "sl_standings"
 * )
 */
class SLStandingsWidget extends SLAdminUIWidgetBase {


}

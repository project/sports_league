<?php

namespace Drupal\sl_club\Plugin\SLAdminUIWidget;

use Drupal\sl_admin_ui\SLAdminUIWidgetBase;

/**
 * Provides a basic widget.
 *
 * @SLAdminUIWidget(
 *   id = "sl_club",
 *   name = @Translation("Clubs"),
 *   description = @Translation("Clubs have teams"),
 *   bundle = "sl_club"
 * )
 */
class SLClubWidget extends SLAdminUIWidgetBase {


}

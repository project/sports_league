<?php

namespace Drupal\sl_stats\Drush\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\sl_stats\SLStatsComputer;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A drush command file.
 *
 * @package Drupal\sl_stats\Commands
 */
class SLStatsCommands extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The SL stats computer.
   *
   * @var \Drupal\sl_stats\SLStatsComputer
   */
  protected $computer;

  /**
   * The lock.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * Construct a SLStatsCommands command.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\sl_stats\SLStatsComputer $sl_stats_computer
   *   The stats computer.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, SLStatsComputer $sl_stats_computer, LockBackendInterface $lock) {
    $this->entityTypeManager = $entity_type_manager;
    $this->computer = $sl_stats_computer;
    $this->lock = $lock;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('sl_stats.computer'),
      $container->get('lock'),
    );
  }

  /**
   * Drush command that resets stats for players and seasons.
   *
   * @command sl_stats:reset
   * @usage sl_stats:reset
   */
  public function reset() {
    $this->computer->reset();
  }

  /**
   * Drush command that generates stats for players and seasons.
   *
   * @param string $id
   *   The player id to compute.
   *
   * @command sl_stats:compute
   * @usage sl_stats:compute
   */
  public function computes($id) {
    if ($this->lock->acquire('sl_stats_autostats')) {
      $this->computer->compute($id);
      $this->lock->release('sl_stats_autostats');
    }
  }

}

<?php

namespace Drupal\sl_stats;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\node\NodeInterface;
use Drupal\sl_stats\Entity\SLStats;

/**
 * Abstract class for SL Stats computer.
 */
abstract class SLStatsComputerFull extends SLStatsComputerBase {

  /**
   * The team type.
   *
   * @var string
   */
  protected string $teamStatsType;

  /**
   * The view id for matches.
   *
   * @var string
   */
  protected string $matchesView;

  /**
   * The display id for matches.
   *
   * @var string
   */
  protected string $matchesDisplayId;

  /**
   * The view id for moments.
   *
   * @var string
   */
  protected string $momentsView;

  /**
   * The display id for moments.
   *
   * @var string
   */
  protected string $momentsDisplayId;

  /**
   * The competitions.
   *
   * @var array
   */
  protected $competitions = [];

  /**
   * The friendlies.
   *
   * @var array
   */
  protected $friendlies = [];

  /**
   * The matches.
   *
   * @var array
   */
  protected $viewMatches = [];

  /**
   * The moments.
   *
   * @var array
   */
  protected $viewMoments = [];

  /**
   * Used to compute intermediate results.
   *
   * @var array
   */
  protected $totalTeam = [];

  /**
   * Gets the totals.
   */
  public function getTotals() {

  }

  /**
   * Pre-compute the stat.
   *
   * @param \Drupal\node\NodeInterface $player
   *   The player.
   * @param \Drupal\node\NodeInterface $team
   *   The team.
   */
  public function preCompute(NodeInterface $player, NodeInterface $team) {
    if (empty($this->matchesView)) {
      return;
    }
    // Computes all matches.
    $this->viewMatches = $this->getViewsResults($this->matchesView, $this->matchesDisplayId, [
      $player->id(),
      $team->id(),
    ]);

    if (!empty($this->viewMatches)) {
      $this->totalTeam['matches'] = count($this->viewMatches) ?? 0;
    }
    else {
      $this->totalTeam['matches'] = 0;
    }

    if (!empty($this->momentsView)) {
      // Computes all moments.
      $this->viewMoments = $this->getViewsResults($this->momentsView, $this->momentsDisplayId, [
        $player->id(),
        $team->id(),
      ]);
    }
  }

  /**
   * Analyzes the match.
   *
   * @param object $match
   *   The match.
   * @param \Drupal\node\NodeInterface $player
   *   The player.
   * @param \Drupal\node\NodeInterface $team
   *   The team.
   */
  protected function analyzeMatch(object $match, NodeInterface $player, NodeInterface $team) {

  }

  /**
   * Creates the stats entity.
   *
   * @param \Drupal\sl_stats\Entity\SLStats $entity
   *   The entity.
   * @param array $values
   *   The values.
   * @param int $person_id
   *   The person id.
   * @param int $team_id
   *   The team id.
   * @param int|null $competition_id
   *   The competition id.
   */
  abstract public function createEntity(SLStats $entity, array $values, int $person_id, int $team_id, ?int $competition_id = NULL);

  /**
   * Computes the entity.
   *
   * @param \Drupal\node\NodeInterface $player
   *   The player.
   * @param \Drupal\node\NodeInterface $team
   *   The team.
   */
  public function compute(NodeInterface $player, NodeInterface $team) {
    $this->competitions = $this->friendlies = $this->totalTeam = [];
    // Computes all matches done in this team.
    $this->preCompute($player, $team);

    // Iterate on all match moments.
    if (!empty($this->viewMoments)) {
      foreach ($this->viewMoments as $match) {
        $match = (object) $match;

        // Non official matches.
        if ($match->field_sl_stats_disabled === '1') {
          if (empty($this->friendlies['unofficial_moments'][$match->type])) {
            $this->friendlies['unofficial_moments'][$match->type] = 1;
          }
          else {
            $this->friendlies['unofficial_moments'][$match->type]++;
          }

          if (!isset($this->totalTeam['unofficial_moments'][$match->type])) {
            $this->totalTeam['unofficial_moments'][$match->type] = 1;
          }
          else {
            $this->totalTeam['unofficial_moments'][$match->type]++;
          }

        }
        else {
          if (empty($this->competitions[$match->field_sl_competition[0]]) || empty($this->competitions[$match->field_sl_competition[0]]['moments'][$match->type])) {
            $this->competitions[$match->field_sl_competition[0]]['moments'][$match->type] = 1;
          }
          else {
            $this->competitions[$match->field_sl_competition[0]]['moments'][$match->type]++;
          }

          if (!isset($this->totalTeam['moments'][$match->type])) {
            $this->totalTeam['moments'][$match->type] = 1;
          }
          else {
            $this->totalTeam['moments'][$match->type]++;
          }
        }
      }
    }

    $total_matches = $friendly_matches = 0;
    $this->friendlies['matches'] = $this->friendlies['minutes'] = 0;
    // Iterate on all matches.
    if (!empty($this->viewMatches)) {
      foreach ($this->viewMatches as $match) {
        $match = (object) $match;

        $this->analyzeMatch($match, $player, $team);

        if ($match->field_sl_stats_disabled === '1') {
          if (!isset($this->friendlies['unofficial_matches'])) {
            $this->friendlies['unofficial_matches'] = 0;
          }
          $this->friendlies['unofficial_matches']++;
          $friendly_matches++;
        }
        else {
          if (isset($match->field_sl_roster_in) && isset($match->field_sl_roster_in)) {
            $in = $match->field_sl_roster_in;
            $out = $match->field_sl_roster_out;

            // Find match time.
            if (!is_numeric($in) || !is_numeric($out)) {
              $time = 0;
            }
            else {
              $time = $out - $in;
            }

            if ($time < 0) {
              $time = 0;
            }

            if (!isset($this->totalTeam['minutes'])) {
              $this->totalTeam['minutes'] = 0;
            }
            $this->totalTeam['minutes'] += $time;

            if (!isset($this->competitions[$match->field_sl_competition[0]]['minutes'])) {
              $this->competitions[$match->field_sl_competition[0]]['minutes'] = 0;
            }

            $this->competitions[$match->field_sl_competition[0]]['minutes'] += $time;
          }

          $total_matches++;

          if (!isset($this->competitions[$match->field_sl_competition[0]]['matches'])) {
            $this->competitions[$match->field_sl_competition[0]]['matches'] = 0;
          }
          $this->competitions[$match->field_sl_competition[0]]['matches']++;
        }
      }
      $this->totalTeam['matches'] = $total_matches;
      $this->totalTeam['unofficial_matches'] = $friendly_matches;
    }

    // Depends on the stats entity type.
    $stats_manager = $this->entityTypeManager->getStorage('sl_stats');
    $entity = $stats_manager->create(['type' => $this->teamStatsType]);
    $this->createEntity($entity, $this->totalTeam, $player->id(), $team->id());
    $this->moduleHandler->alter('sl_stats_person', $entity, $player);
    if ($entity instanceof ContentEntityInterface) {
      $entity->save();
    }

    if (!isset($player->total_stats['matches'])) {
      $player->total_stats['matches'] = 0;
    }
    $player->total_stats['matches'] += $this->totalTeam['matches'];

    if (!isset($player->total_stats['goals'])) {
      $player->total_stats['goals'] = 0;
    }
    $player->total_stats['goals'] += empty($this->totalTeam['moments']['sl_match_moments_goal']) ? 0 : $this->totalTeam['moments']['sl_match_moments_goal'];
    $this->postCompute($player, $team);
  }

  /**
   * Post compute the stats.
   *
   * @param \Drupal\node\NodeInterface $player
   *   The player.
   * @param \Drupal\node\NodeInterface $team
   *   The team.
   */
  public function postCompute(NodeInterface $player, NodeInterface $team) {
    $stats_manager = $this->entityTypeManager->getStorage('sl_stats');
    if (!empty($this->competitions)) {
      foreach ($this->competitions as $key => $value) {
        if (is_numeric($key)) {
          $entity_c = $stats_manager->create(['type' => $this->teamStatsType]);
          $this->createEntity($entity_c, $value, $player->id(), $team->id(), $key);
          $this->moduleHandler->alter('sl_stats_person', $entity_c, $node);
          $entity_c->save();
        }
      }
    }

    if (!empty($this->friendlies) && !empty($this->teamStatsTypeNonOfficial)) {
      $entity_c = $stats_manager->create(['type' => $this->teamStatsTypeNonOfficial]);
      $this->createEntity($entity_c, $this->friendlies, $player->id(), $team->id());
      $this->moduleHandler->alter('sl_stats_person', $entity_c, $node);
      $entity_c->save();
    }
  }

}

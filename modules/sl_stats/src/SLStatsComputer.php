<?php

namespace Drupal\sl_stats;

use Drupal\Component\Utility\Timer;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * The stats computer service.
 */
class SLStatsComputer {

  /**
   * The entity type manager.
   *
   * @var \Drupal\sl_stats\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The queue.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * The plugin manager.
   *
   * @var \Drupal\sl_stats\SLStatsComputerManager
   */
  protected $pluginManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * SLStatsComputer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue.
   * @param \Drupal\sl_stats\SLStatsComputerManager $plugin_manager
   *   The plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueueFactory $queue, SLStatsComputerManager $plugin_manager, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queue = $queue;
    $this->pluginManager = $plugin_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Resets the computer.
   */
  public function reset() {
    Timer::start('sl_stats_reset');
    $stop = 1;
    ini_set('max_execution_time', 0);
    ini_set('memory_limit', '1024M');

    // Iterate over all players.
    $efq = $this->entityTypeManager->getStorage('node')->getQuery();
    $efq->condition('type', 'sl_person');
    $efq->condition('status', 1);
    $efq->accessCheck(TRUE);
    $result = $efq->execute();
    /** @var QueueInterface $queue */
    $queue = $this->queue->get('sl_stats_worker');

    if (!empty($result)) {
      foreach ($result as $entity) {
        $item = new \stdClass();
        $item->nid = $entity;
        $queue->createItem($item);
      }
    }
  }

  /**
   * Computes the stats for a person.
   *
   * @param int $person_id
   *   The person id.
   */
  public function compute(int $person_id) {
    ini_set("memory_limit", "-1");
    Timer::start('sl_stats_compute');
    $computer_plugins = $this->pluginManager->getDefinitions();
    $node_manager = $this->entityTypeManager->getStorage('node');
    $stats_manager = $this->entityTypeManager->getStorage('sl_stats');
    foreach ($computer_plugins as $name => $plugin) {
      $computers[$name] = $this->pluginManager->createInstance($name);
    }

    if (!empty($person_id)) {
      $node = $node_manager->load($person_id);
    }

    $efq = $stats_manager->getQuery();

    if (!empty($node)) {
      // Deletes existing stats.
      $efq->condition('field_sl_stats_person', $node->id());
      $efq->condition('type', 'sl_stats_manual', '<>');
      $efq->accessCheck(TRUE);
      $result_stats = $efq->execute();
      $stats = $stats_manager->loadMultiple(array_values($result_stats));
      $stats_manager->delete($stats);
    }

    $teams = [];
    $total_stats['matches'] = $total_stats['goals'] = 0;
    if (!empty($node->field_sl_teams)) {
      foreach ($node->field_sl_teams as $team) {
        $teams[] = $team;

        // Don't compute for teams with manual stats present.
        $efq = $stats_manager->getQuery();
        $efq->condition('type', 'sl_stats_manual');
        $efq->condition('field_sl_stats_person', $node->id());
        $efq->condition('field_sl_teams', $team->entity->id());
        $efq->accessCheck(TRUE);
        $manual_stats = $efq->execute();
        if (!empty($manual_stats)) {
          continue;
        }

        // Defines the computer.
        foreach ($computer_plugins as $name => $plugin) {
          if ($computers[$name]->isApplicable($node, $team->entity)) {
            $computers[$name]->compute($node, $team->entity);
            continue 2;
          }
        }
      }
    }

    if (!empty($node)) {
      $this->moduleHandler->alter('sl_stats_finish', $node, $node->total_stats);
      $node->sl_stats_already_computed = TRUE;
      $node->save();
      return $node;
    }
  }

}

<?php

declare(strict_types=1);

namespace Drupal\sl_stats;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of sports league stats type entities.
 *
 * @see \Drupal\sl_stats\Entity\SLStatsType
 */
final class SLStatsTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No sports league stats types available. <a href=":link">Add sports league stats type</a>.',
      [':link' => Url::fromRoute('entity.sl_stats_type.add_form')->toString()],
    );

    return $build;
  }

}

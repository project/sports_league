<?php

declare(strict_types=1);

namespace Drupal\sl_stats\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the sports league stats entity class.
 *
 * @ContentEntityType(
 *   id = "sl_stats",
 *   label = @Translation("Stats"),
 *   label_collection = @Translation("Stats"),
 *   label_singular = @Translation("stats"),
 *   label_plural = @Translation("stats"),
 *   label_count = @PluralTranslation(
 *     singular = "@count stats",
 *     plural = "@count stats",
 *   ),
 *   bundle_label = @Translation("Type"),
 *   handlers = {
 *     "list_builder" = "Drupal\sl_stats\SLStatsListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\sl_stats\SLStatsAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\sl_stats\Form\SLStatsForm",
 *       "edit" = "Drupal\sl_stats\Form\SLStatsForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\sl_base\Routing\SLHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "sl_stats",
 *   data_table = "sl_stats_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer sl",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "bundle" = "type",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/sl-stats",
 *     "add-form" = "/sl-stats/add/{sl_stats_type}",
 *     "add-page" = "/sl-stats/add",
 *     "canonical" = "/sl-stats/{sl_stats}",
 *     "edit-form" = "/sl-stats/{sl_stats}",
 *     "delete-form" = "/sl-stats/{sl_stats}/delete",
 *     "delete-multiple-form" = "/admin/content/sl-stats/delete-multiple",
 *   },
 *   bundle_entity_type = "sl_stats_type",
 *   field_ui_base_route = "entity.sl_stats_type.edit_form",
 * )
 */
final class SLStats extends ContentEntityBase {

}

<?php

declare(strict_types=1);

namespace Drupal\sl_stats\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Sports League Stats type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "sl_stats_type",
 *   label = @Translation("Sports League Stats type"),
 *   label_collection = @Translation("Sports League Stats types"),
 *   label_singular = @Translation("sports league stats type"),
 *   label_plural = @Translation("sports league statss types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count sports league statss type",
 *     plural = "@count sports league statss types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\sl_stats\Form\SLStatsTypeForm",
 *       "edit" = "Drupal\sl_stats\Form\SLStatsTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\sl_stats\SLStatsTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer sl_stats types",
 *   bundle_of = "sl_stats",
 *   config_prefix = "sl_stats_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/sl_stats_types/add",
 *     "edit-form" = "/admin/structure/sl_stats_types/manage/{sl_stats_type}",
 *     "delete-form" = "/admin/structure/sl_stats_types/manage/{sl_stats_type}/delete",
 *     "collection" = "/admin/structure/sl_stats_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class SLStatsType extends ConfigEntityBundleBase {

  /**
   * The machine name of this sports league stats type.
   */
  protected string $id;

  /**
   * The human-readable name of the sports league stats type.
   */
  protected string $label;

}

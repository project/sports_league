<?php

namespace Drupal\sl_stats\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'SLStats Person' block.
 *
 * @Block(
 *  id = "slstats_person",
 *  admin_label = @Translation("SL Stats Person"),
 * )
 */
class SLStatsPerson extends BlockBase {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a SLMatchTabs object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The language manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $current_route_match, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $current_route_match;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $content = [];

    // To be used in sl_person node page.
    if ($node = $this->currentRouteMatch->getParameter('node')) {
      if ($node->bundle() == 'sl_person') {

        $stats_manager = $this->entityTypeManager->getStorage('sl_stats');

        // Check what type of stats will be shown.
        $efq = $stats_manager->getQuery();
        $efq->condition('field_sl_stats_person', $node->id());
        $efq->accessCheck(TRUE);
        $stats_ids = $efq->execute();

        if (!empty($stats_ids)) {
          $results = $this->database
            ->select('sl_stats')
            ->fields('sl_stats', ['id', 'type'])
            ->condition('id', $stats_ids, 'IN')
            ->execute()
            ->fetchAll();

          foreach ($results as $key => $stats) {
            if ($stats->type == 'sl_stats_player') {
              $block = 'block_1';
            }
            else {
              if ($stats->type == 'sl_stats_coach') {
                $block = 'block_3';
              }
              else {
                if ($stats->type == 'sl_stats_mini') {
                  $block = 'block_5';
                }
              }
            }
          }

          // Show them.
          $view = Views::getView('sl_stats_players');
          if (is_object($view)) {
            $view->setArguments([$node->id()]);
            $view->setDisplay($block);
            $view->preExecute();
            $view->execute();
            $view->buildRenderable($block, [$node->id()]);
            $content = $view->render();
          }
        }
      }
    }
    $build = [];
    $build['slstats_person'] = $content;

    return $build;
  }

}

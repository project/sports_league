<?php

namespace Drupal\sl_stats\Plugin\SLStatsComputer;

use Drupal\sl_stats\SLStatsComputerFull;

/**
 * Provides a basic widget.
 *
 * @SLStatsComputer(
 *   id = "sl_stats_player",
 *   name = @Translation("SL Stats Player"),
 *   description = @Translation("SL Stats Player"),
 * )
 */
class SLStatsPlayer extends SLStatsComputerFull {
  /**
   * {@inheritdoc}
   */

  protected string $teamStatsType = 'sl_stats_player';

  /**
   * {@inheritdoc}
   */
  protected string $teamStatsTypeNonOfficial = 'sl_stats_player_non_official';

  /**
   * {@inheritdoc}
   */
  protected string $matchesView = 'sl_stats_matches_computing';

  /**
   * {@inheritdoc}
   */
  protected string $matchesDisplayId = 'attachment_1';

  /**
   * {@inheritdoc}
   */
  protected string $momentsView = 'sl_stats_match_moments_computing';

  /**
   * {@inheritdoc}
   */
  protected string $momentsDisplayId = 'attachment_1';

  /**
   * {@inheritdoc}
   */
  public function getTotals() {

  }

  /**
   * {@inheritdoc}
   */
  protected function getCoachPositionTermId() {
    $efq = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery()
      ->condition('vid', 'sl_person_positions')
      ->accessCheck(FALSE)
      ->condition('field_sl_is_coach_position', '1');

    $results = $efq->execute();
    if (!empty($results)) {
      return reset($results);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable($player, $team) {
    if ($this->getTeamStatsType($team) == 'sl_stats_player' && $player->field_sl_person_position->target_id !== $this->getCoachPositionTermId()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function createEntity($entity, $values, $person_id, $team_id, $competition_id = NULL, $matches_unofficial = 0, $goals_unofficial = 0) {
    $entity->set('field_sl_stats_matches', $values['matches'] ?? 0);
    $entity->set('field_sl_stats_minutes', $values['minutes'] ?? 0);
    $entity->set('field_sl_stats_goals', !empty($values['moments']['sl_match_moments_goal']) ? $values['moments']['sl_match_moments_goal'] : 0);
    $entity->set('field_sl_stats_yellow_cards', !empty($values['moments']['sl_match_moments_yellow_card']) ? $values['moments']['sl_match_moments_yellow_card'] : 0);
    $entity->set('field_sl_stats_red_cards', !empty($values['moments']['sl_match_moments_red_card']) ? $values['moments']['sl_match_moments_red_card'] : 0);
    $entity->set('field_sl_teams', $team_id);
    $entity->set('field_sl_stats_person', $person_id);
    if (in_array($entity->bundle(), ['sl_stats_player', 'sl_stats_player_non_official'])) {
      $entity->set('field_sl_stats_unofficial_goals', !empty($values['unofficial_moments']['sl_match_moments_goal']) ? $values['unofficial_moments']['sl_match_moments_goal'] : 0);
      $entity->set('field_sl_stats_unofficial_matche', !empty($values['unofficial_matches']) ? $values['unofficial_matches'] : 0);
    }
    if (!empty($competition_id)) {
      $entity->set('field_sl_stats_competition', $competition_id);
    }

  }

}

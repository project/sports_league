<?php

namespace Drupal\sl_stats\Plugin\SLStatsComputer;

use Drupal\sl_stats\SLStatsComputerFull;

/**
 * Provides a basic widget.
 *
 * @SLStatsComputer(
 *   id = "sl_stats_coach",
 *   name = @Translation("SL Stats Coach"),
 *   description = @Translation("SL Stats Coach"),
 * )
 */
class SLStatsCoach extends SLStatsComputerFull {

  /**
   * {@inheritdoc}
   */
  protected string $teamStatsType = 'sl_stats_coach';

  /**
   * {@inheritdoc}
   */
  protected string $matchesView = 'sl_stats_matches_computing';

  /**
   * {@inheritdoc}
   */
  protected string $matchesDisplayId = 'attachment_1';

  /**
   * {@inheritdoc}
   */
  public function getTotals() {

  }

  /**
   * {@inheritdoc}
   */
  protected function getCoachPositionTermId() {
    $efq = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery()
      ->condition('vid', 'sl_person_positions')
      ->accessCheck(FALSE)
      ->condition('field_sl_is_coach_position', '1');

    $results = $efq->execute();
    if (!empty($results)) {
      return reset($results);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable($player, $team) {
    if ($this->getTeamStatsType($team) == 'sl_stats_player' && $player->field_sl_person_position->target_id == $this->getCoachPositionTermId()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function analyzeMatch($match, $player, $team) {
    // Non-official matches are not analyzed.
    if ($match->field_sl_stats_disabled === '1') {
      return;
    }

    if ($match->field_sl_match_score_home > $match->field_sl_match_score_away) {
      $result = 'win';
    }
    elseif ($match->field_sl_match_score_away > $match->field_sl_match_score_home) {
      $result = 'lost';
    }
    elseif ($match->field_sl_goals_home_penalties > $match->field_sl_goals_away_penalties) {
      $result = 'win';
    }
    elseif ($match->field_sl_goals_away_penalties > $match->field_sl_goals_home_penalties) {
      $result = 'lost';
    }
    else {
      $result = 'draw';
    }

    if ($team->id() == $match->field_sl_match_team_away) {
      if ($result == 'win') {
        $result = 'lost';
      }
      elseif ($result == 'lost') {
        $result = 'win';
      }
    }

    if (empty($this->competitions) || empty($this->competitions[$match->field_sl_competition[0]]) || empty($this->competitions[$match->field_sl_competition[0]][$result])) {
      $this->competitions[$match->field_sl_competition[0]][$result] = 1;
    }
    else {
      $this->competitions[$match->field_sl_competition[0]][$result]++;
    }

    if (!isset($this->totalTeam[$result])) {
      $this->totalTeam[$result] = 1;
    }
    else {
      $this->totalTeam[$result]++;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createEntity($entity, $values, $person_id, $team_id, $competition_id = NULL) {
    $entity->set('field_sl_stats_match_wins', !empty($values['win']) ? $values['win'] : 0);
    $entity->set('field_sl_stats_match_lost', !empty($values['lost']) ? $values['lost'] : 0);
    $entity->set('field_sl_stats_match_draws', !empty($values['draw']) ? $values['draw'] : 0);
    $entity->set('field_sl_stats_match_perc_win', !empty($values['win']) ? round($values['win'] / $values['matches'], 2) : 0);
    $entity->set('field_sl_stats_match_perc_lost', !empty($values['lost']) ? round($values['lost'] / $values['matches'], 2) : 0);
    $entity->set('field_sl_stats_match_perc_draws', !empty($values['draw']) ? round($values['draw'] / $values['matches'], 2) : 0);
    $entity->set('field_sl_stats_matches', $values['matches']);
    $entity->set('field_sl_teams', $team_id);
    $entity->set('field_sl_stats_person', $person_id);

    if (!empty($competition_id)) {
      $entity->set('field_sl_stats_competition', $competition_id);
    }
  }

}

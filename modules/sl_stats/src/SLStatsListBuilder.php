<?php

declare(strict_types=1);

namespace Drupal\sl_stats;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the sports league stats entity type.
 */
final class SLStatsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['person'] = $this->t('Person');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\sl_stats\SLStatsInterface $entity */
    $row['id'] = $entity->id();
    $row['type'] = $entity->bundle();
    $row['person'] = '';
    if ($entity->field_sl_stats_person->entity) {
      $row['person'] = ['data' => $entity->field_sl_stats_person->entity->toLink()];
    }
    return $row + parent::buildRow($entity);
  }

}

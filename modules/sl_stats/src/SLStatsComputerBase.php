<?php

namespace Drupal\sl_stats;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for sl_stats computer.
 */
abstract class SLStatsComputerBase extends PluginBase implements SLStatsComputerPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a SLStatsComputerBase object.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManager $entityTypeManager, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entityTypeManager;
    $this->moduleHandler = $module_handler;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * Whether the plugin is applicable.
   */
  abstract public function isApplicable($player, $team);

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->pluginDefinition['name'];
  }

  /**
   * Fetches raw results from a view.
   *
   * @param string $view_name
   *   The view name.
   * @param string $display_id
   *   The display id.
   * @param array $args
   *   The args.
   *
   * @return array
   *   The view results in a keyed array.
   */
  protected function getViewsResults(string $view_name, string $display_id, array $args): array {
    $values = [];
    $view = Views::getview($view_name);
    if ($view) {
      $view->setDisplay($display_id);
      $view->setArguments($args);
      $view->execute();
      foreach ($view->result as $rid => $row) {
        foreach ($view->field as $fid => $field) {
          $values[$rid][$fid] = $field->getValue($row);
        }
      }
    }
    return $values;
  }

  /**
   * Get the team stats type.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The team.
   *
   * @return string|null The team stats type.
   *   The team stats type.
   */
  protected function getTeamStatsType(NodeInterface $node): ?string {

    // Direct.
    if (!empty($node->field_sl_stats_type->value)) {
      return $node->field_sl_stats_type->value;
    }
    // Parent team.
    elseif (!empty($node->field_sl_teams->entity->field_sl_stats_type)) {
      return $node->field_sl_teams->entity->field_sl_stats_type->value;
    }
    return '';
  }

}

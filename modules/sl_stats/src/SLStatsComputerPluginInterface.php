<?php

namespace Drupal\sl_stats;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines an interface for reusable form plugins.
 */
interface SLStatsComputerPluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Return the name of the reusable form plugin.
   *
   * @return string
   *   The name.
   */
  public function getName(): string;

}

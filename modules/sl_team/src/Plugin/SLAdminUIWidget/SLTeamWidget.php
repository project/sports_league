<?php

namespace Drupal\sl_team\Plugin\SLAdminUIWidget;

use Drupal\sl_admin_ui\SLAdminUIWidgetBase;

/**
 * Provides a basic widget.
 *
 * @SLAdminUIWidget(
 *   id = "sl_team",
 *   name = @Translation("Teams"),
 *   description = @Translation("Teams play in matches"),
 *   bundle = "sl_team"
 * )
 */
class SLTeamWidget extends SLAdminUIWidgetBase {


}

# Sports League
Sports League module is an out of the box solution to manage sports league functionality in Drupal. It relies on popular modules in drupal to help manage content typically used in websites for sports clubs. Main example is derived from football clubs, but can be easily extended.

## Functionality included:
-   Manage competitions, multi competition editions and its standings.
-   Manage clubs and teams.
-   Manage matches, rosters and match moments.
-   Manage players.
-   Manage automatic statistics on players and teams.
-   Manage club titles.

Developed for serbenfiquista.com